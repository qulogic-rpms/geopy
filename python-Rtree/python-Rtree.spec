%global srcname Rtree

Name:           python-%{srcname}
Version:        0.8.3
Release:        3%{?dist}
Summary:        R-Tree spatial index for Python GIS
%global _description \
Rtree is a ctypes Python wrapper of the spatialindex library, that \
provides a number of advanced spatial indexing features for the \
spatially curious Python user. These features include: \
\
-Nearest neighbor search \
-Intersection search \
-Multi-dimensional indexes \
-Clustered indexes (store Python pickles directly with index entries) \
-Bulk loading \
-Deletion \
-Disk serialization \
-Custom storage implementation \
 (to implement spatial indexing in ZODB, for example)

License:        LGPLv2
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://files.pythonhosted.org/packages/source/R/%{srcname}/%{srcname}-%{version}.tar.gz
# https://github.com/Toblerity/rtree/pull/85
Patch0001:      %{srcname}-0001-TST-Fix-test-errors-and-bugs.patch

BuildArch:      noarch
BuildRequires:  spatialindex-devel >= 1.7.9
BuildRequires:  python2-devel python3-devel

BuildRequires:  python2-pytest python3-pytest
BuildRequires:  python2-numpy python3-numpy

BuildRequires:  python3-sphinx

%description %{_description}


%package -n python2-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python2-%{srcname}}

Requires:       spatialindex >= 1.7.9

%description -n python2-%{srcname} %{_description}


%package -n python3-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python3-%{srcname}}

Requires:       spatialindex >= 1.7.9

%description -n python3-%{srcname} %{_description}


%prep
%autosetup -n %{srcname}-%{version} -p1

# Delete junk from tarball.
rm -rf %{srcname}.egg-info
find . -name '*.pyc' -delete
rm setup.cfg
rm -r docs/build


%build
%py2_build
%py3_build

pushd docs
SPHINXBUILD=sphinx-build-%{python3_version} \
    make html
popd

%install
# Must do the python2 install first because the scripts in /usr/bin are
# overwritten with every setup.py install, and in general we want the
# python3 version to be the default.
# If, however, we're installing separate executables for python2 and python3,
# the order needs to be reversed so the unversioned executable is the python2
# one.
%py2_install
%py3_install


%check
export LANG=C.UTF-8
PYTHONPATH="%{buildroot}%{python3_sitelib}" \
    py.test-%{python3_version} -ra tests
PYTHONPATH="%{buildroot}%{python3_sitelib}" \
    py.test-%{python3_version} -ra --doctest-modules rtree
PYTHONPATH="%{buildroot}%{python2_sitelib}" \
    py.test-%{python2_version} -ra tests
PYTHONPATH="%{buildroot}%{python2_sitelib}" \
    py.test-%{python2_version} -ra --doctest-modules rtree


# Note that there is no %%files section for the unversioned python module if we
# are building for several python runtimes
%files -n python2-%{srcname}
%license LICENSE.txt
%doc README.md
%{python2_sitelib}/rtree
%{python2_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE.txt
%doc README.md
%{python3_sitelib}/rtree
%{python3_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Sat Aug 12 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.8.3-3
- Simplify spec with more macros.

* Sun Mar 05 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.8.3-2
- NumPy is needed for testing as well

* Sun Mar 05 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.8.3-1
- New upstream release
- Update spec to new guidelines
- Add Python 3 subpackage
- Add documentation

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.7.0-10
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.0-9
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.7.0-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.0-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sat Feb 08 2014 Volker Fröhlich <volker27@gmx.at> - 0.7.0-5
- Remove hard-coded library extension (BZ#1001840)
- Ignore harmless test failure to fix FTBFS
- Remove obsolete version requirements

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Fri Oct 12 2012 Volker Fröhlich <volker27@gmx.at> - 0.7.0-2
- BR python-setuptools instead of ...-devel
- Delete pre-built egg info

* Sat Apr 14 2012 Volker Fröhlich <volker27@gmx.at> - 0.7.0-1
- Initial package for Fedora
