%global srcname branca

Name:           python-%{srcname}
Version:        0.2.0
Release:        2%{?dist}
Summary:        Generate complex HTML+JS pages with Python
%global _description \
This library is a spinoff from folium, that would host the non-map-specific \
features. It may become a HTML+JS generation library in the future. It is based \
on Jinja2 only.

License:        MIT
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://files.pythonhosted.org/packages/source/b/%{srcname}/%{srcname}-%{version}.tar.gz
Patch0001:      %{srcname}-0001-Use-Firefox-driver-instead-of-PhantomJS.patch

BuildArch:      noarch

BuildRequires:  python2-devel python3-devel
BuildRequires:  python2-jinja2 python3-jinja2

BuildRequires:  python2-pytest python3-pytest
BuildRequires:  python2-flake8 python3-flake8
BuildRequires:  python2-selenium python3-selenium
BuildRequires:  firefox
BuildRequires:  xorg-x11-server-Xvfb

%description %{_description}


%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

%description -n python2-%{srcname} %{_description}


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

%description -n python3-%{srcname} %{_description}


%prep
%autosetup -n %{srcname}-%{version} -p1


%build
%py2_build
%py3_build


%install
# Must do the python2 install first because the scripts in /usr/bin are
# overwritten with every setup.py install, and in general we want the
# python3 version to be the default.
# If, however, we're installing separate executables for python2 and python3,
# the order needs to be reversed so the unversioned executable is the python2
# one.
%py2_install
%py3_install


%check
xvfb-run %{__python2} setup.py test
xvfb-run %{__python3} setup.py test


# Note that there is no %%files section for the unversioned python module if we
# are building for several python runtimes
%files -n python2-%{srcname}
%license LICENSE.txt
%doc README.rst
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE.txt
%doc README.rst
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Sat Aug 12 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.2.0-2
- Simplify spec with more macros.

* Fri Mar 10 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.2.0-1
- Initial package release.
