%global srcname click-plugins
%global srcname_no_dash click_plugins

Name:           python-%{srcname}
Version:        1.0.3
Release:        3%{?dist}
Summary:        Click extension to register CLI commands via setuptools
%global _description \
An extension module for click to register external CLI commands via setuptools \
entry-points.

License:        MIT
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://files.pythonhosted.org/packages/source/c/%{srcname}/%{srcname}-%{version}.tar.gz

BuildArch:      noarch

%description %{_description}


%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

BuildRequires:  python2-devel
BuildRequires:  python2-click >= 3.0
BuildRequires:  python2-pytest
Requires:       python2-click >= 3.0

%description -n python2-%{srcname} %{_description}


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

BuildRequires:  python3-devel
BuildRequires:  python3-click >= 3.0
BuildRequires:  python3-pytest
Requires:       python3-click >= 3.0

%description -n python3-%{srcname} %{_description}


%prep
%autosetup -n %{srcname}-%{version}


%build
%py2_build
%py3_build


%install
%py2_install
%py3_install


%check
export LANG=C.UTF-8
PYTHONPATH="%{buildroot}%{python3_sitelib}" \
    py.test-%{python3_version} -ra
PYTHONPATH="%{buildroot}%{python2_sitelib}" \
    py.test -ra


%files -n python2-%{srcname}
%license LICENSE.txt
%doc README.rst
%{python2_sitelib}/%{srcname_no_dash}
%{python2_sitelib}/%{srcname_no_dash}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE.txt
%doc README.rst
%{python3_sitelib}/%{srcname_no_dash}
%{python3_sitelib}/%{srcname_no_dash}-%{version}-py?.?.egg-info


%changelog
* Sat Aug 12 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.3-3
- Simplify spec with more macros.

* Sun Jul 09 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.3-2
- Simplify spec a bit.

* Sat Mar 04 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.3-1
- Initial package release.
