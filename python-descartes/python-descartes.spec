%global srcname descartes

Name:           python-%{srcname}
Version:        1.1.0
Release:        3%{?dist}
Summary:        Use geometric objects as Matplotlib paths and patches
%global _description \
A Python module that allows using Shapely or GeoJSON-like geometric objects as \
Matplotlib paths and patches.

License:        BSD
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://files.pythonhosted.org/packages/source/d/%{srcname}/%{srcname}-%{version}.tar.gz

BuildArch:      noarch

%description %{_description}


%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

BuildRequires:  python2-devel
BuildRequires:  python2-numpy
BuildRequires:  python2-matplotlib
# Missing python2-shapely.
BuildRequires:  python-shapely
Requires:       python2-numpy
Requires:       python2-matplotlib

%description -n python2-%{srcname} %{_description}


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

BuildRequires:  python3-devel
BuildRequires:  python3-numpy
BuildRequires:  python3-matplotlib
BuildRequires:  python3-shapely
Requires:       python3-numpy
Requires:       python3-matplotlib

%description -n python3-%{srcname} %{_description}


%prep
%autosetup -n %{srcname}-%{version}


%build
%py2_build
%py3_build


%install
%py2_install
%py3_install


%check
%{__python2} setup.py test
%{__python3} setup.py test


%files -n python2-%{srcname}
%doc README.txt
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%doc README.txt
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Sat Aug 12 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.0-3
- Simplify spec with more macros.

* Sun Jul 09 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.0-2
- Simplify spec a bit.

* Sat Mar 04 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.0-1
- Initial package release.
