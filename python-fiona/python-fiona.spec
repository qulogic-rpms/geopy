%global srcname fiona
%global Srcname Fiona

Name:           python-%{srcname}
Version:        1.7.8
Release:        3%{?dist}
Summary:        Read and write spatial data files
%global _description \
Fiona is designed to be simple and dependable. It focuses on reading and \
writing data in standard Python IO style and relies upon familiar Python types \
and protocols such as files, dictionaries, mappings, and iterators instead of \
classes specific to OGR. Fiona can read and write real-world data using \
multi-layered GIS formats and zipped virtual file systems and integrates \
readily with other Python GIS packages such as pyproj, Rtree, and Shapely.

License:        BSD
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://github.com/Toblerity/%{Srcname}/archive/%{version}/%{Srcname}-%{version}.tar.gz
# https://github.com/Toblerity/Fiona/pull/427
Patch0001:      %{srcname}-0001-TST-Fix-flaky-ls-test.patch

BuildRequires:  gdal-devel >= 1.8
BuildRequires:  /usr/bin/ogrinfo

%description %{_description}


%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

BuildRequires:  python2-devel
BuildRequires:  python2-Cython
BuildRequires:  python2-six
BuildRequires:  python-munch
BuildRequires:  python-cligj
BuildRequires:  python2-click-plugins
BuildRequires:  python-shapely
BuildRequires:  python-enum34

BuildRequires:  python2-nose
BuildRequires:  python2-pytest

Requires:       python2-six
Requires:       python-munch
Requires:       python-cligj
Requires:       python2-click-plugins
Requires:       python-shapely
Requires:       python-enum34

%description -n python2-%{srcname} %{_description}


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

BuildRequires:  python3-devel
BuildRequires:  python3-Cython
BuildRequires:  python3-six
BuildRequires:  python3-munch
BuildRequires:  python3-cligj
BuildRequires:  python3-click-plugins
BuildRequires:  python3-shapely

BuildRequires:  python3-nose
BuildRequires:  python3-pytest

Requires:       python3-six
Requires:       python3-munch
Requires:       python3-cligj
Requires:       python3-click-plugins
Requires:       python3-shapely

%description -n python3-%{srcname} %{_description}


%prep
%autosetup -n %{Srcname}-%{version} -p1


%build
%py2_build
%py3_build


%install
%py2_install
%py3_install


%check
export LANG=C.UTF-8

mkdir temp
pushd temp
ln -s ../tests
PYTHONPATH="%{buildroot}%{python3_sitearch}" \
    nosetests-%{python3_version} --exclude test_filter_vsi --exclude test_geopackage --exclude test_write_mismatch
PYTHONPATH="%{buildroot}%{python2_sitearch}" \
    nosetests --exclude test_filter_vsi --exclude test_geopackage --exclude test_write_mismatch
popd


%files -n python2-%{srcname}
%license LICENSE.txt
%doc README.rst CHANGES.txt CREDITS.txt
%{python2_sitearch}/%{srcname}
%{python2_sitearch}/%{Srcname}-%{version}-py%{python2_version}.egg-info


%files -n python3-%{srcname}
%license LICENSE.txt
%doc README.rst CHANGES.txt CREDITS.txt
%{_bindir}/fio
%{python3_sitearch}/%{srcname}
%{python3_sitearch}/%{Srcname}-%{version}-py%{python3_version}.egg-info


%changelog
* Sat Aug 12 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.7.8-3
- Simplify spec with more macros.

* Sun Jul 09 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.7.8-2
- Restore flaky test patch (not applied upstream yet)

* Sun Jul 09 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.7.8-1
- Simplify spec a bit.
- Update fiona to latest release.

* Sun Mar 05 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.7.4-3
- Fix flaky ls test.

* Sun Mar 05 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.7.4-2
- Testing requires pytest as well.

* Sat Mar 04 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.7.4-1
- Initial package release.
