%global srcname folium
%global sum Make beautiful maps with Leaflet.js & Python

Name:           python-%{srcname}
Version:        0.3.0
Release:        2%{?dist}
Summary:        %{sum}
%global _description \
Folium builds on the data wrangling strengths of the Python ecosystem and the \
mapping strengths of the Leaflet.js library. Manipulate your data in Python, \
then visualize it in on a Leaflet map via Folium.

License:        MIT
URL:            https://python-visualization.github.io/folium/
Source0:        https://github.com/python-visualization/%{srcname}/archive/v%{version}/%{srcname}-%{version}.tar.gz
Patch0001:      %{srcname}-0001-Remove-vincent-examples.patch
Patch0002:      %{srcname}-0002-Replace-seawater-with-geographiclib.patch

BuildArch:      noarch
BuildRequires:  python2-devel python3-devel
BuildRequires:  python2-six python3-six
BuildRequires:  python2-jinja2 python3-jinja2
BuildRequires:  python2-branca python3-branca

BuildRequires:  python2-pytest python3-pytest
BuildRequires:  python2-numpy python3-numpy
BuildRequires:  python2-pandas python3-pandas
BuildRequires:  python2-flake8 python3-flake8
BuildRequires:  python2-mock
BuildRequires:  python2-nbconvert python3-nbconvert
BuildRequires:  python2-jupyter-client python3-jupyter-client
BuildRequires:  python2-ipykernel python3-ipykernel
BuildRequires:  python2-geopandas python3-geopandas
BuildRequires:  python2-cartopy python3-cartopy
BuildRequires:  python2-gpxpy python3-gpxpy
BuildRequires:  python-GeographicLib python3-GeographicLib
BuildRequires:  python2-mplleaflet python3-mplleaflet

%description %{_description}


%package -n python2-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python2-%{srcname}}

Requires:       python2-six
Requires:       python2-jinja2
Requires:       python2-branca

%description -n python2-%{srcname} %{_description}


%package -n python3-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python3-%{srcname}}

Requires:       python3-six
Requires:       python3-jinja2
Requires:       python3-branca

%description -n python3-%{srcname} %{_description}


%prep
%autosetup -n %{srcname}-%{version} -p1


%build
%py2_build
%py3_build


%install
# Must do the python2 install first because the scripts in /usr/bin are
# overwritten with every setup.py install, and in general we want the
# python3 version to be the default.
# If, however, we're installing separate executables for python2 and python3,
# the order needs to be reversed so the unversioned executable is the python2
# one.
%py2_install
%py3_install


%check
PYTHONPATH=%{buildroot}%{python3_sitelib} \
    py.test-%{python3_version} -ra
PYTHONPATH=%{buildroot}%{python2_sitelib} \
    py.test-%{python2_version} -ra


# Note that there is no %%files section for the unversioned python module if we
# are building for several python runtimes
%files -n python2-%{srcname}
%license LICENSE.txt
%doc README.rst
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE.txt
%doc README.rst
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Sat Aug 12 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.0-2
- Simplify spec with more macros.

* Fri Mar 10 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.0-1
- New upstream release.

* Sat Mar 04 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.2.1-1
- Initial package release.
