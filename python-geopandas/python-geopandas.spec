%global srcname geopandas

Name:           python-%{srcname}
Version:        0.3.0
Release:        2%{?dist}
Summary:        Geographic Pandas extensions
%global _description \
GeoPandas is a project to add support for geographic data to Pandas objects. \
\
The goal of GeoPandas is to make working with geospatial data in Python easier. \
It combines the capabilities of Pandas and Shapely, providing geospatial \
operations in Pandas and a high-level interface to multiple geometries to \
Shapely. GeoPandas enables you to easily do operations in Python that would \
otherwise require a spatial database such as PostGIS.

License:        BSD
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://github.com/%{srcname}/%{srcname}/archive/v%{version}/%{srcname}-%{version}.tar.gz
# https://github.com/geopandas/geopandas/pull/551 for both
Patch0001:      0001-Fix-default-cmap-for-Matplotlib-2.0.0.patch
Patch0002:      0002-TST-Fix-determination-of-expected-dash-style.patch
# https://github.com/geopandas/geopandas/issues/552
Patch0003:      0003-Workaround-test-issues-on-non-x86-systems.patch

BuildArch:      noarch

%description %{_description}


%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

BuildRequires:  python2-devel
BuildRequires:  python2-six >= 1.3.0
BuildRequires:  python2-descartes >= 1.0
BuildRequires:  python2-fiona >= 1.0.1
BuildRequires:  python2-pandas
BuildRequires:  pyproj >= 1.9.3
BuildRequires:  python-shapely >= 1.2.18

BuildRequires:  python2-pytest
BuildRequires:  python2-rtree >= 0.8
BuildRequires:  python2-mock >= 1.0.1
BuildRequires:  python2-matplotlib >= 1.2.1
# Still needed in this release of Matplotlib.
BuildRequires:  python2-nose python3-nose

Requires:       python2-six >= 1.3.0
Requires:       python2-descartes >= 1.0
Requires:       python2-fiona >= 1.0.1
Requires:       python2-pandas
Requires:       pyproj >= 1.9.3
Requires:       python-shapely >= 1.2.18

%description -n python2-%{srcname} %{_description}


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

BuildRequires:  python3-devel
BuildRequires:  python3-six >= 1.3.0
BuildRequires:  python3-descartes
BuildRequires:  python3-fiona >= 1.0.1
BuildRequires:  python3-pandas
BuildRequires:  python3-pyproj >= 1.9.3
BuildRequires:  python3-shapely >= 1.2.18

BuildRequires:  python3-pytest
BuildRequires:  python3-rtree >= 0.8
BuildRequires:  python3-matplotlib >= 1.2.1
# Still needed in this release of Matplotlib.
BuildRequires:  python3-nose

Requires:       python3-six >= 1.3.0
Requires:       python3-descartes
Requires:       python3-fiona >= 1.0.1
Requires:       python3-pandas
Requires:       python3-pyproj >= 1.9.3
Requires:       python3-shapely >= 1.2.18

%description -n python3-%{srcname} %{_description}


%prep
%autosetup -n %{srcname}-%{version} -p1


%build
%py2_build
%py3_build


%install
%py2_install
%py3_install


%check
PYTHONPATH="%{buildroot}%{python3_sitelib}" \
    py.test-%{python3_version} -ra geopandas
PYTHONPATH="%{buildroot}%{python2_sitelib}" \
    py.test-%{python2_version} -ra geopandas


%files -n python2-%{srcname}
%license LICENSE.txt
%doc README.md CHANGELOG.md
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE.txt
%doc README.md CHANGELOG.md
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Fri Dec 01 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.0-2
- Add patch for non-x86 systems.

* Tue Sep 19 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.0-1
- Update to latest version.
- Use lowercase rtree dependency.
- Fix inconsistent capitalization.

* Sat Aug 12 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.2.1-3
- Simplify spec with more macros.

* Sun Jul 09 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.2.1-2
- Simplify spec a bit.
- Add patch for new Pandas compatibility.

* Sun Mar 05 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.2.1-1
- Initial package release.
