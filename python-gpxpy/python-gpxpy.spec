%global srcname gpxpy

Name:           python-%{srcname}
Version:        1.1.2
Release:        2%{?dist}
Summary:        GPX file parser and GPS track manipulation library
%global _description \
This is a simple python library for parsing and manipulating GPX files. GPX is \
an XML based format for GPS tracks.

License:        ASL 2.0
URL:            https://pypi.python.org/pypi/%{srcname}
# https://github.com/tkrajina/gpxpy/pull/88 - with tests
Source0:        %{srcname}-%{version}.tar.gz
# Source0:        https://files.pythonhosted.org/packages/source/g/%%{srcname}/%%{srcname}-%%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  python2-devel python3-devel
BuildRequires:  python2-lxml python3-lxml

%description %{_description}


%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

Requires:       python2-lxml

%description -n python2-%{srcname} %{_description}


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

Requires:       python3-lxml

%description -n python3-%{srcname} %{_description}


%prep
%autosetup -n %{srcname}-%{version}


%build
%py2_build
%py3_build


%install
# Must do the python2 install first because the scripts in /usr/bin are
# overwritten with every setup.py install, and in general we want the
# python3 version to be the default.
# If, however, we're installing separate executables for python2 and python3,
# the order needs to be reversed so the unversioned executable is the python2
# one.
%py2_install
%py3_install


%check
%{__python2} test.py
%{__python3} test.py


# Note that there is no %%files section for the unversioned python module if we
# are building for several python runtimes
%files -n python2-%{srcname}
%license LICENSE.txt
%doc README.md
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE.txt
%doc README.md
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py?.?.egg-info
%{_bindir}/gpxinfo


%changelog
* Sat Aug 12 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.2-2
- Simplify spec with more macros.

* Fri Mar 10 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.2-1
- Initial package release.
