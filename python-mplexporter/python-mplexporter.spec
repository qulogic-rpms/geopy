%global srcname mplexporter
%global commit 474a3a49fe3b3dca859f69a6173263018dd2d34c
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:           python-%{srcname}
Version:        0.0.1
Release:        0.5.20170329git%{shortcommit}%{?dist}
Summary:        A general scraper/exporter for Matplotlib plots
%global _description \
A proof of concept general Matplotlib exporter.

License:        BSD
URL:            https://github.com/mpld3/mplexporter
Source0:        https://github.com/mpld3/mplexporter/archive/%{commit}/%{name}-%{commit}.tar.gz
Patch0001:      0001-Fix-broken-tests-on-Mpl-2.patch

BuildArch:      noarch

%description %{_description}


%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}

BuildRequires:  python2-devel
BuildRequires:  python2-setuptools
BuildRequires:  python2-numpy
BuildRequires:  python2-matplotlib
BuildRequires:  python2-pandas

# Tests
BuildRequires:  python2-jinja2
BuildRequires:  python2-nose

Requires:       python2-numpy
Requires:       python2-matplotlib

%description -n python2-%{srcname} %{_description}


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-numpy
BuildRequires:  python3-matplotlib
BuildRequires:  python3-pandas

# Tests
BuildRequires:  python3-jinja2
BuildRequires:  python3-nose

Requires:       python3-numpy
Requires:       python3-matplotlib

%description -n python3-%{srcname} %{_description}


%prep
%autosetup -n %{srcname}-%{commit} -p1


%build
%py2_build
%py3_build


%install
%py2_install
%py3_install


%check
PYTHONPATH="%{buildroot}%{python3_sitelib}" MPLBACKEND=Agg \
    nosetests-%{python3_version} mplexporter
PYTHONPATH="%{buildroot}%{python2_sitelib}" MPLBACKEND=Agg \
    nosetests-%{python2_version} mplexporter


%files -n python2-%{srcname}
%license LICENSE
%doc README.md
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE
%doc README.md
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Sat Aug 12 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.0.1-0.5.20170329git474a3a4
- Simplify spec with more macros.

* Thu Jul 13 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.0.1-0.4.20170329git474a3a4
- Update to new snapshot.
- Clean up spec a bit.
- Fix broken tests with Mpl 2.

* Fri Mar 10 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.0.1-0.3.20161101gita6bb9fe
- Resync with latest Python spec template
- Update to latest snapshot

* Thu May 28 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.0.1-0.2.20150528git837428a
- Remove pandas BR where unavailable
- Pre-set Matplotlib backend for testing

* Thu May 28 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.0.1-0.1.20150528git837428a
- Initial RPM release
