%global srcname mplleaflet

Name:           python-%{srcname}
Version:        0.0.5
Release:        2%{?dist}
Summary:        Convert Matplotlib plots into Leaflet web maps
%global _description \
mplleaflet is a Python library that converts a matplotlib plot into a webpage \
containing a pannable, zoomable Leaflet map. It can also embed the Leaflet map \
in an IPython notebook. The goal of mplleaflet is to enable use of Python and \
matplotlib for visualizing geographic data on slippy maps without having to \
write any Javascript or HTML. You also don't need to worry about choosing the \
base map content i.e., coastlines, roads, etc.

License:        BSD
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://github.com/jwass/%{srcname}/archive/v%{version}/%{srcname}-%{version}.tar.gz
Patch0001:      %{srcname}-0001-Unvendor-mplexporter.patch

BuildArch:      noarch

BuildRequires:  python2-devel python3-devel
BuildRequires:  python2-six python3-six
BuildRequires:  python2-jinja2 python3-jinja2
BuildRequires:  python2-mplexporter python3-mplexporter
BuildRequires:  python2-matplotlib python3-matplotlib

BuildRequires:  python2-pytest python3-pytest

%description %{_description}


%package -n python2-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python2-%{srcname}}

Requires:       python2-six
Requires:       python2-jinja2
Requires:       python2-mplexporter
Requires:       python2-matplotlib

%description -n python2-%{srcname} %{_description}


%package -n python3-%{srcname}
Summary:        %{sum}
%{?python_provide:%python_provide python3-%{srcname}}

Requires:       python3-six
Requires:       python3-jinja2
Requires:       python3-mplexporter
Requires:       python3-matplotlib

%description -n python3-%{srcname} %{_description}


%prep
%autosetup -n %{srcname}-%{version} -p1


%build
%py2_build
%py3_build


%install
# Must do the python2 install first because the scripts in /usr/bin are
# overwritten with every setup.py install, and in general we want the
# python3 version to be the default.
# If, however, we're installing separate executables for python2 and python3,
# the order needs to be reversed so the unversioned executable is the python2
# one.
%py2_install
%py3_install


%check
PYTHONPATH="%{buildroot}%{python3_sitelib}" MPLBACKEND=Agg \
    py.test-%{python3_version} tests
PYTHONPATH="%{buildroot}%{python2_sitelib}" MPLBACKEND=Agg \
    py.test-%{python2_version} tests


# Note that there is no %%files section for the unversioned python module if we
# are building for several python runtimes
%files -n python2-%{srcname}
%license LICENSE.md
%doc README.md
%{python2_sitelib}/%{srcname}
%{python2_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%files -n python3-%{srcname}
%license LICENSE.md
%doc README.md
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Sat Aug 12 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.0.5-2
- Simplify spec with more macros.

* Fri Mar 10 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.0.5-1
- Initial package release.
